﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    public class UserInfo
    {
        public UserInfo(string p_Name, string p_Role)
        {
            m_name = p_Name;
            m_role = p_Role;
        }

        public int GetRoleLevel() { return Functions.GetRoleLevel(m_role); }

        private string m_name = "";
        private string m_role = "";
    }
}
