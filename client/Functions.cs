﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace client
{
    public class Functions
    {
        static private Dictionary<string, int> m_Roles = new Dictionary<string, int>() { { "", 0 }, { "гр1", 1 }, { "гр2", 2 }, { "гр3", 3 }, { "гр4", 4 } };
        public enum AuthReslut
        {
            RESULT_OK = 0,
            RESULT_ERROR_LOGIN,
            RESULT_ERROR_PASSWORD,
            RESULT_ERROR_SQL,
            RESULT_ERROR_LOGIN_OR_PASSWORD,
            RESULT_ERROR_UNKNOWN_ROLE
        }
        static public AuthReslut Auth(string login, string password, ref string role)
        {
            if (!ValidateLogin(login))
                return AuthReslut.RESULT_ERROR_LOGIN;

            if (!ValidatePassword(password))
                return AuthReslut.RESULT_ERROR_PASSWORD;

            SqlDataReader reader = SQL.CreateCommand(SQL.QI.SELECT_ACCOUNT, login, password).ExecuteReader();
            if (reader == null)
                return AuthReslut.RESULT_ERROR_SQL;

            if (reader.Read())
            {
                role = reader.GetString(0);
                reader.Close();
                if (string.IsNullOrWhiteSpace(role))
                    return AuthReslut.RESULT_ERROR_LOGIN_OR_PASSWORD;
                if (!ValidRole(role))
                    return AuthReslut.RESULT_ERROR_UNKNOWN_ROLE;
            }
            else
            {
                reader.Close();
                return AuthReslut.RESULT_ERROR_LOGIN_OR_PASSWORD;
            }
            return AuthReslut.RESULT_OK;
        }

        static public bool ValidateLogin(string login)
        {
            return !String.IsNullOrWhiteSpace(login);
        }

        static public bool ValidatePassword(string password)
        {
            return !String.IsNullOrWhiteSpace(password);
        }

        static public int GetRoleLevel(string p_Role) { return m_Roles[p_Role]; }

        static public bool ValidRole(string p_Role)
        {
            return m_Roles.TryGetValue(p_Role, out int info);
        }

        static public void SendMessageFromResult(AuthReslut p_Result)
        {
            switch (p_Result)
            {
                case AuthReslut.RESULT_OK:
                    break;
                case AuthReslut.RESULT_ERROR_LOGIN:
                    MessageBox.Show("Введите корректрый логин");
                    break;
                case AuthReslut.RESULT_ERROR_PASSWORD:
                    MessageBox.Show("Введите корректрый пароль");
                    break;
                case AuthReslut.RESULT_ERROR_SQL:
                    MessageBox.Show("Ошибка при обработке запроса");
                    break;
                case AuthReslut.RESULT_ERROR_LOGIN_OR_PASSWORD:
                    MessageBox.Show("Неверный логин или пароль");
                    break;
                case AuthReslut.RESULT_ERROR_UNKNOWN_ROLE:
                    MessageBox.Show("На данном аккаунте установлен неправильный уровень доступа");
                    break;
                default:
                    MessageBox.Show("Непредвиденная ошибка");
                    break;
            }
        }

        static public void FillComboBox(ComboBox p_ComboBox, SQL.QI p_Query, params object[] args)
        {
            SqlDataAdapter l_Adapter = SQL.CreateDataAdapter(p_Query, args);
            DataSet l_DS = new DataSet();
            l_Adapter.Fill(l_DS, "comboBox");
            p_ComboBox.DisplayMember = "login";
            p_ComboBox.ValueMember = "id";
            p_ComboBox.DataSource = l_DS.Tables["comboBox"];
        }

        static public void LoadComboBox(ComboBox p_ComboBox, SQL.QI p_Query, params object[] args)
        {
            SqlDataReader l_Reader = SQL.CreateCommand(SQL.QI.SELECT_ALL_ACCOUNTS, args).ExecuteReader();
            DataTable l_dt = new DataTable();
            l_dt.Columns.Add("id", typeof(String));
            l_dt.Columns.Add("name", typeof(String));
            l_dt.Load(l_Reader);
            p_ComboBox.ValueMember = "id";
            p_ComboBox.DisplayMember = "login";
            p_ComboBox.DataSource = l_dt;
        }

        static public void LoadTestComboBox(ComboBox p_ComboBox, SQL.QI p_Query, params object[] args)
        {
            SqlDataReader l_Reader = SQL.CreateCommand(SQL.QI.SELECT_ALL_ACCOUNTS, args).ExecuteReader();
            while(l_Reader.Read())
            {
                p_ComboBox.Items.Add(new test(l_Reader["id"].ToString(), l_Reader["login"].ToString()));
            }
            l_Reader.Close();
            p_ComboBox.ValueMember = "m_id";
            p_ComboBox.DisplayMember = "m_login";
        }

        static public void LoadDataGrid(DataGridView p_DataGridView, SQL.QI p_Query, params object[] args)
        {
            SqlDataAdapter l_SqlDataAdapter = SQL.CreateDataAdapter(p_Query, args);
            DataSet l_DataSet = new DataSet();
            l_SqlDataAdapter.Fill(l_DataSet);
            p_DataGridView.DataSource = l_DataSet.Tables[0];
        }

        static public void SaveToCSV(List<String> p_Text)
        {
            StreamWriter sw = new StreamWriter("test.csv");
            for (int i = 0; i < p_Text.Count; i++)
                sw.WriteLine(p_Text[0]);

            sw.Close();
        }

        static public void LoadCSV(DataGridView p_DataGridView, string p_Path)
        {
            StreamReader sr = new StreamReader(p_Path);

            string line = "";
            string[] cells;

            p_DataGridView.Columns.Add("Id", "ID");
            p_DataGridView.Columns.Add("Login", "Login");
            p_DataGridView.Columns.Add("Password", "Password");
            p_DataGridView.Columns.Add("Role", "Role");
            p_DataGridView.Columns.Add("Name", "Name");


            while ((line = sr.ReadLine()) != null)
            {
                cells = line.Split(';');
                string id = cells[0];
                string login = cells[1];
                string password = cells[2];
                string role = cells[3];
                string name = cells[4];

                p_DataGridView.Rows.Add(cells);
            }

            sr.Close();
        }

        class test
        {
            public test(string id, string login)
            {
                m_id = id;
                m_login = login;
            }

            public string m_id { get; set; }
            public string m_login { get; set; }
        }
    }
}
