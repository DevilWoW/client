﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Auth : Form
    {
        public Auth()
        {
            InitializeComponent();
        }

        public string role = "";

        private void button1_Click(object sender, EventArgs e)
        {
            Functions.AuthReslut result = Functions.Auth(textBox1.Text, textBox2.Text, ref role);

            if (result == Functions.AuthReslut.RESULT_OK)
            {
                UserInfo l_UserInfo = new UserInfo(textBox1.Text, role);
                Main main = new Main();
                main.m_userInfo = l_UserInfo;
                main.Owner = this;
                main.Show();
                Hide();
            }
            Functions.SendMessageFromResult(result);
        }
    }
}
