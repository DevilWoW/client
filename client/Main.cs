﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace client
{
    public partial class Main : Form
    {
        public UserInfo m_userInfo;
        public Main()
        {
            InitializeComponent();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Owner.Show();
            m_userInfo = null;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            switch (m_userInfo.GetRoleLevel())
            {
                case 1:
                    tabControl1.Controls.Remove(tabPage2);
                    tabControl1.Controls.Remove(tabPage3);
                    tabControl1.Controls.Remove(tabPage4);
                    break;
                case 2:
                    tabControl1.Controls.Remove(tabPage1);
                    tabControl1.Controls.Remove(tabPage3);
                    tabControl1.Controls.Remove(tabPage4);
                    break;
                case 3:
                    tabControl1.Controls.Remove(tabPage1);
                    tabControl1.Controls.Remove(tabPage2);
                    tabControl1.Controls.Remove(tabPage4);
                    break;
                case 4:
                    tabControl1.Controls.Remove(tabPage1);
                    tabControl1.Controls.Remove(tabPage2);
                    tabControl1.Controls.Remove(tabPage3);
                    break;
                default:
                    break;
            }

            Functions.LoadTestComboBox(comboBox1, SQL.QI.SELECT_ALL_ACCOUNTS);
            Functions.LoadDataGrid(dataGridView1, SQL.QI.SELECT_ACC_NAME);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                string id = dataGridView1[0, i].Value.ToString();
                string login = dataGridView1[1, i].Value.ToString();
                string password = dataGridView1[2, i].Value.ToString();
                string role = dataGridView1[3, i].Value.ToString();
                string name = dataGridView1[4, i].Value.ToString();

                SqlCommand l_Cmd1 = SQL.CreateCommand(SQL.QI.SAVE_ACCOUNT, login, password, role, id);
                SqlCommand l_Cmd2 = SQL.CreateCommand(SQL.QI.SAVE_ACCOUNT_INFO, name, id);
                l_Cmd1.ExecuteNonQuery();
                l_Cmd2.ExecuteNonQuery();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<String> l_Text = new List<string>();

            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                string id = dataGridView1[0, i].Value.ToString();
                string login = dataGridView1[1, i].Value.ToString();
                string password = dataGridView1[2, i].Value.ToString();
                string role = dataGridView1[3, i].Value.ToString();
                string name = dataGridView1[4, i].Value.ToString();

                l_Text.Add(id + ";" + login + ";" + password + ";" + role + ";" + name);
            }

            Functions.SaveToCSV(l_Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            Functions.LoadCSV(dataGridView2, openFileDialog1.FileName);
        }
    }
}
