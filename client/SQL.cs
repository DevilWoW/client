﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace client
{
    public class SQL
    {
        public enum QI
        {
            SELECT_ACCOUNT = 1,
            SELECT_ALL_ACCOUNTS,
            SELECT_ACC_NAME,
            SAVE_ACCOUNT,
            SAVE_ACCOUNT_INFO
        }

        private static Dictionary<QI, string> PrepareStatement = new Dictionary<QI, string>();

        static SqlConnection connection = new SqlConnection();

        static public void ConnectToDB()
        {
            connection.Close();
            connection.ConnectionString = Properties.Settings.Default.ConnectionString;
            connection.Open();
        }

        static public SqlConnection GetDB()
        {
            return connection;
        }

        static public void LoadDataBase()
        {
            ConnectToDB();
            LoadPrepareStatement();
        }

        static public SqlCommand CreateCommand(QI query, params object[] args)
        {
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = GetCommandText(query, args);
            return cmd;
        }

        static public SqlCommand CreateCommand(string CommandText)
        {
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = CommandText;
            return cmd;
        }

        static public SqlDataAdapter CreateDataAdapter(QI query, params object[] args)
        {
            return new SqlDataAdapter(GetCommandText(query, args), GetDB());
        }

        static public void LoadPrepareStatement()
        {
            PrepareStatement.Clear();
            PrepareStatement.Add(QI.SELECT_ACCOUNT, "SELECT role FROM [user] WHERE login = '{0}' and password = '{1}'");
            PrepareStatement.Add(QI.SELECT_ALL_ACCOUNTS, "SELECT id, login from [user]");
            PrepareStatement.Add(QI.SELECT_ACC_NAME, "select u.id as ID, u.login as Login, u.password as Password, u.role as Role, ui.name as Name FROM [user] u INNER JOIN [user_info] ui ON ui.id = u.id");
            PrepareStatement.Add(QI.SAVE_ACCOUNT, "UPDATE [user] SET login = '{0}', password = '{1}', role = '{2}' WHERE id = '{3}'");
            PrepareStatement.Add(QI.SAVE_ACCOUNT_INFO, "UPDATE [user_info] SET name = '{0}' WHERE id = '{1}'");
        }

        static public string GetCommandText(QI query, params object[] args)
        {
            return @String.Format(PrepareStatement[query], args);
        }
    }
}
